import _ from 'underscore';
import $ from 'jquery';
import Backbone from 'Backbone';
import BlockCollection from '../collections/BlockCollection';

/**
 * Carousel view.
 */

var CarouselView = Backbone.View.extend({
  // The DOM Element associated with this view
  el: '.carousel',
  template: _.template(require('../templates/CarouselTemplate.html')),

  // By default display 4 elements.
  blocksToDisplay: 4,

  // Initial current block
  currentBlock: 0,

  // View Event Handlers
  events: {
    'click .prev': 'onPrev',
    'click .next': 'onNext',
  },

  initialize: function () {
    // Create our block collection.
    this.blocks = new BlockCollection();

    // Fetch the collection blocks from the server and render the view.
    this.blocks.fetch({ success: _.bind(this.render, this) });
  },

  // Renders the view's template to the UI
  render: function () {
    // Dynamically updates the UI with the view's template
    this.$el.html(this.template({
      blocks: this.blocks.models,
      currentBlock: this.currentBlock,
      blocksToDisplay: this.blocksToDisplay,
    }));

    // Initialize group to show and enabled buttons
    this.setButtonsState(this.blocksToDisplay);

    // Maintains chainability
    return this;
  },

  onPrev: function () {
    this.setCurrentBlockGroup(-this.blocksToDisplay);
    return this;
  },

  onNext: function () {
    this.setCurrentBlockGroup(this.blocksToDisplay);
    return this;
  },

  setCurrentBlockGroup: function (newCurrentBlock) {
    // Remove the current class
    this.$('#block-group' + this.currentBlock).removeClass('current');

    // Adding or removing current block
    this.currentBlock = this.currentBlock + newCurrentBlock;

    // Add the current class
    this.$('#block-group' + this.currentBlock).addClass('current');

    // Sets the prev next buttons disable state
    this.setButtonsState(newCurrentBlock);
  },

  // Enables/disables the next/prev buttons
  setButtonsState: function (newCurrentBlock) {
    // Current block is the first one
    if (this.currentBlock == 0) {
      this.$('.prev').prop('disabled', true);
    } else {
      this.$('.prev').prop('disabled', false);
    }

    // Next current block is at the end
    if (this.currentBlock + newCurrentBlock >= this.blocks.models.length) {
      this.$('.next').prop('disabled', true);
    } else {
      this.$('.next').prop('disabled', false);
    }
  },
});

export default CarouselView;
