import Backbone from 'Backbone';

/**
 * Block model.
 */
var BlockModel = Backbone.Model.extend({

  // Default values for all of the Model attributes
  defaults: {
    title: 'String',
    images: [],
  },

  // Gets a random image from the images list
  getRandomImage: function () {
    var index = Math.floor(Math.random() * this.get('images').length);
    return this.get('images')[index];
  },
});

export default BlockModel;
