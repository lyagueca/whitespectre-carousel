import Backbone from 'Backbone';
import BlockModel from '../models/BlockModel';

/**
 * Block collection model.
 */
var BlockCollection = Backbone.Collection.extend({
  model: BlockModel,
  url: '/blocks',
});

export default BlockCollection;
