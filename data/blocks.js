module.exports = [{
  title: 'Fruit',
  images: [
    'http://www.thenewpotato.com/wp-content/uploads/2016/04/mango-miracle-fruit-100x100.jpg?x28529',
    'http://www.brisbanemarkets.com.au/wp-content/uploads/2013/12/Pineapple-Fruit-110x110.jpg',
    'http://www.thenewpotato.com/wp-content/uploads/2016/04/lime-miracle-fruit-100x100.jpg?x28529',
    'http://vitahound.com/wp-content/uploads/2013/06/banana-bunch.png',
    'https://yt3.ggpht.com/-Obhhu4R53Vw/AAAAAAAAAAI/AAAAAAAAAAA/So2THMnAd_s/s100-c-k-no-mo-rj-c0xffffff/photo.jpg'
    ],
}, {
  title: 'Phone cases',
  images: [
    'https://cdn.shopify.com/s/files/1/0687/4327/products/M97_large.jpg?v=1489538854',
    'http://almatajjer.com/image/cache/catalog/Capdase/iPhone%206%20covers/Capdase-Flip-Case-Cover-For-SDL088421839-1-f4f22-800x640.jpg',
    'https://cdn.shopify.com/s/files/1/0816/8505/products/Henna_grande.JPG?v=1442536177',
    'https://static.decalgirl.com/assets/items/oci5/800/oci5-tiedye.jpg',
    ],
}, {
  title: 'Computer',
  images: [
    'https://www.pickaboo.com/media/catalog/product/cache/1/thumbnail/205x205/9df78eab33525d08d6e5fb8d27136e95/a/p/apple-imac-retina-5k-27-mk462za-a.jpg',
    'http://fuerzadesign.com/wp-content/uploads/2014/01/Fuerza-Computer-150x150.jpg',
    'http://www.waltonbd.com/image/cache/data/Computer/Laptop/Passion/WP157U3G/p1-500x500.jpg',
    'http://www.waltonbd.com/image/cache/data/Computer/Laptop/Passion/WP157U3G/p01-500x500.jpg',
    ],
}, {
  title: 'Hats',
  images: [
    'http://demandware.edgesuite.net/sits_pod49/dw/image/v2/AAKN_PRD/on/demandware.static/-/Sites-bdel/default/dw332641e0/products/headwear/S16/FX7L_972_SLNK_BD_Trucker_Hat_web.jpg?sh=350',
    'http://nflshop.frgimages.com/FFImage/thumb.aspx?i=/productImages/_1019000/ff_1019801_full.jpg&w=340',
    'http://www.outbacktrading.com/img/product/1305-GRY-LG_1.jpg?fv=AD4A0B496937EDAC425B5085FAF9B46B-12590',
    'https://s-media-cache-ak0.pinimg.com/736x/4c/d9/e4/4cd9e4830b8f235cb5133a9fc8669ea2.jpg',
    ],
}, {
  title: 'Bags',
  images: [
    'http://pccircle.com/image/cache/data/Accessoires/Bags/Notebook%20Bag%2017%20HP2.jpg',
    'https://mec.imgix.net/medias/sys_master/high-res/high-res/8863498371102/5035675-UTD00.jpg?w=300&h=300&auto=format&q=40&fit=fill&bg=0FFF',
    'http://pccircle.com/image/cache/data/Accessoires/Bags/Tucano/Tucano%20BDR15BAG%202-500x500.jpg',
    'http://chongaik.com/image/cache/data/items/Bags/ACERBIS/Acerbis-Predator-Bag-500x500.jpg',
    'https://www.laptopmag.com/images/wp/laptop-slideshow/307910.jpg',
    ],
}, {
  title: 'Shoes',
  images: [
    'http://www.forestblu.com/image/cache/data/52/52-M-Shoes/52FB2986-01%20web-300x310.jpg',
    'http://suitored.com/wp-content/uploads/2013/01/Saint-Laurent-Patent-Leather-OxfordShoe.jpg',
    'http://www.redloja.com.br/image/cache/data/Nova1/2014-new-summer-women-sandals-wedge-heel-high-Bohemia-zipper-calceus-National-style-summer-shoes-for9%20(451%20x%20419)-350x350.jpg',
    'http://www.forestblu.com/image/cache/data/52/52-M-Shoes/52FB2981-83%20web-550x582.jpg',
    ],
}, {
  title: 'T-Shirts',
  images: [
    'http://martjackstorage.azureedge.net/in-resources/b368029c-a4dd-448a-a888-58348cb1b144/Images/ProductImages/Zoom/NKEPJK479887410MIDNL.jpg',
    'http://nba.frgimages.com/FFImage/thumb.aspx?i=/productImages/_2404000/ff_2404956_full.jpg&w=340',
    'http://nflshop.frgimages.com/FFImage/thumb.aspx?i=/productImages/_2478000/ff_2478642_full.jpg&w=340',
    'http://www.unixstickers.com/image/data/t-shirt/javascript/javascript%20t-shirt%20detail%203.jpg',
    ],
}, {
  title: 'Pants',
  images: [
    'http://www.klim.com/images/3143-002_Gray_01.jpg?resizeid=3&resizeh=600&resizew=600',
    'http://www.patagonia.com/dis/dw/image/v2/ABBM_PRD/on/demandware.static/-/Sites-patagonia-master/default/dw63dd8968/images/hi-res/21970_DFTG.jpg?sw=750&sh=750&sm=fit&sfrm=png',
    ],
}, {
  title: 'Watches',
  images: [
    'https://www.jaga.gr/wp-content/uploads/cm/data/JAGA-WATCHES/AQ/AQ401-3-567x650.jpg',
    'https://www.jaga.gr/wp-content/uploads/cm/data/JAGA-WATCHES/STAINLESS_16/jaga-j1013-2-j1013-2.jpg',
    'http://ph-live-01.slatic.net/p/7/casio-mens-black-resin-strap-watch-ae-1000w-1bvdf-7564-952835-945ae315eadb4cd664d3bab8f669218b-zoom_850x850.jpg',
    'https://snwatches.com/image/cache/catalog/2016-Citizen-Watch-model-750x750.jpg',
    'https://gloimg.twinkledeals.com/td/pdm-product-pic/Clothing/2017/04/10/goods-img/1491797614704857337.jpg',
    ],
}, {
  title: 'Chairs',
  images: [
    'http://www.resourceoneinc.com/image/cache/data/Chameleon/Gold%20Circle%20Chameleon%20Chair%20Back-400x400.jpg',
    'http://hhcommercial.com.au/image/cache/data/espalda-side-chair-700x700.jpeg',
    'http://www.greenington.com/wp-content/uploads/2016/05/GL0002CA-Laurel-Dining-Chair-Side.jpg',
    'http://www.interiordesignsdirect.co.uk/image/cache/catalog/daw/eames-daw-white-back-750x750.jpg',
    ],
}, {
  title: 'Sunglasses',
  images: [
    'http://odoasem.com/new/image/cache/data/JewelleryAccessories/Accessories/Sunglasses/aevogue333-500x500.PNG',
    'https://gq-images.condecdn.net/image/MpRdErpy6pp/crop/405',
    'http://www.peepculture.com/wp-content/uploads/2012/09/Prada-Shield-Sunglasses.jpg',
    ],
}, {
  title: 'Jackets',
  images: [
    'http://shop.rubynz.com/user/images/7537_300_500.jpg?t=1609150946',
    'http://www.klim.com/images/5146-002_Blue_02.jpg?resizeid=3&resizeh=600&resizew=600',
    'https://s-media-cache-ak0.pinimg.com/736x/30/e5/ac/30e5accbbd620592cb0737a6962e1462.jpg',
    'http://images.footballfanatics.com/FFImage/thumb.aspx?i=/productImages/_2513000/ff_2513593a_full.jpg&w=340',
    ],
},
];
