var express = require('express');
var app = express();
var router = express.Router();
const PORT = 8080;

// Configuration of the data blocks
var dataBlocks = require('./data/blocks');

app.use(express.static([__dirname, '/public'].join('')));

/* API to generate carosel blocks */
app.get('/blocks', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.json(dataBlocks);
});

app.listen(PORT, () => console.info('Running server: http://localhost:' + PORT));
